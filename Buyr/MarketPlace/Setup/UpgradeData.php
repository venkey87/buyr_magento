<?php
namespace Buyr\MarketPlace\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class UpgradeData implements UpgradeDataInterface
{
	private $eavSetupFactory;

	public function __construct(EavSetupFactory $eavSetupFactory)
	{
		$this->eavSetupFactory = $eavSetupFactory;
	}

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		//$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'upc');

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'upc',
			[
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'UPC',
				'input' => 'text',
				'class' => '',
				'source' => '',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '',
				'searchable' => true,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => true,
				'unique' => false,
				'apply_to' => ''
			]
		);


	}
	
}