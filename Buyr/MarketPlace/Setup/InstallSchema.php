<?php
namespace Buyr\MarketPlace\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('buyr_seller_info')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('buyr_seller_info')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Seller ID'
            )
            ->addColumn(
                'buyr_retailer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'buyr_retailer_id'
            )
            ->addColumn(
                'authentication_key',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'buyr_retailer_id'
            )
            ->addColumn(
                'buyr_retailer_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'buyr_retailer_status'
            )
            ->addColumn(
                'email',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'email'
            )
            ->addColumn(
                'firstname',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'firstname'
            )
            ->addColumn(
                'lastname',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'lastname'
            )
            ->addColumn(
                'password',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'password'
            )
            ->addColumn(
                'entity_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'entity_name'
            )
            ->addColumn(
                'trade_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'trade_name'
            )
            ->addColumn(
                'ein_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'ein_number'
            )
            ->addColumn(
                'business_phone',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'business_phone'
            )
            ->addColumn(
                'adminPhone',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'adminPhone'
            )
            ->addColumn(
                'address_line_1',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'address_line_1'
            )
            ->addColumn(
                'address_line_2',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'address_line_2'
            )
            ->addColumn(
                'city',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'city'
            )
            ->addColumn(
                'state',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'state'
            )
            ->addColumn(
                'zip',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'zip'
            )
            ->addColumn(
                'product_origin_address_line_1',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'product_origin_address_line_1'
            )
            ->addColumn(
                'product_origin_address_line_2',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'product_origin_address_line_2'
            )
            ->addColumn(
                'product_origin_city',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'product_origin_city'
            )
            ->addColumn(
                'product_origin_state',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'product_origin_state'
            )
            ->addColumn(
                'product_origin_zip',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'product_origin_zip'
            )
            ->addColumn(
                'routing_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'routing_number'
            )
            ->addColumn(
                'account_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'account_number'
            )
            ->addColumn(
                'name_on_card',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'name_on_card'
            )
            ->addColumn(
                'card_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'card_number'
            )
            ->addColumn(
                'cvv',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'cvv'
            )
            ->addColumn(
                'expiration_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'expiration_date'
            )
            ->addColumn(
                'subcription_model',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'subcription_model'
            )
            ->addColumn(
                'retailer_image',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'retailer_image'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [],
                'Seller Created At'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [],
                'Seller Updated At'
            )
            ->setComment('Seller Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('buyr_seller_info'),
                $setup->getIdxName(
                    $installer->getTable('buyr_seller_info'),
                    ['buyr_retailer_id','buyr_retailer_status','email','firstname','lastname','password','entity_name','trade_name','ein_number','business_phone','adminPhone','address_line_1','address_line_2','city','state','zip','product_origin_address_line_1','product_origin_address_line_2','product_origin_city','product_origin_state','product_origin_zip','routing_number','account_number','name_on_card','card_number','cvv','expiration_date','retailer_image'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['buyr_retailer_id','buyr_retailer_status','email','firstname','lastname','password','entity_name','trade_name','ein_number','business_phone','adminPhone','address_line_1','address_line_2','city','state','zip','product_origin_address_line_1','product_origin_address_line_2','product_origin_city','product_origin_state','product_origin_zip','routing_number','account_number','name_on_card','card_number','cvv','expiration_date','retailer_image'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        if (!$installer->tableExists('buyr_seller_view')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('buyr_seller_view')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'View ID'
            )
            ->addColumn(
                'view_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'view_name'
            )
            ->addColumn(
                'idstarting',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'idstarting'
            )
            ->addColumn(
                'idending',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'idending'
            )
            ->addColumn(
                'pricestarting',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'pricestarting'
            )
            ->addColumn(
                'priceending',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'priceending'
            )
            ->addColumn(
                'qtystarting',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'qtystarting'
            )
            ->addColumn(
                'qtyending',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'qtyending'
            )
            ->addColumn(
                'visibility',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'visibility'
            )
            ->addColumn(
                'attributeset',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'attributeset'
            )
            ->addColumn(
                'col_pos_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'col_pos_id'
            )
			->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'sku'
            )
			->addColumn(
                'visibilitystatus',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'visibilitystatus'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'status'
            )
            ->setComment('View Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('buyr_seller_view'),
                $setup->getIdxName(
                    $installer->getTable('buyr_seller_view'),
                    ['view_name','idstarting','idending','pricestarting','priceending','qtystarting','qtyending','visibility','attributeset','col_pos_id','sku','visibilitystatus','status'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['view_name','idstarting','idending','pricestarting','priceending','qtystarting','qtyending','visibility','attributeset','col_pos_id','sku','visibilitystatus','status'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        if (!$installer->tableExists('buyr_view_column_position')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('buyr_view_column_position')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'View Column Position ID'
            )
            ->addColumn(
                'column_names',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'column_names'
            )
            ->addColumn(
                'positions',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'positions'
            )
            ->setComment('View Column Position Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('buyr_view_column_position'),
                $setup->getIdxName(
                    $installer->getTable('buyr_view_column_position'),
                    ['column_names','positions'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['column_names','positions'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        if (!$installer->tableExists('buyr_attribute_column')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('buyr_attribute_column')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'View Column Position ID'
            )
            ->addColumn(
                'column_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'column_code'
            )
            ->addColumn(
                'column_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'column_id'
            )
            ->addColumn(
                'column_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'column_name'
            )
            ->addColumn(
                'position',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'position'
            )
			->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'status'
            )
            ->setComment('View Column Position Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('buyr_attribute_column'),
                $setup->getIdxName(
                    $installer->getTable('buyr_attribute_column'),
                    ['column_code','column_id','column_name','position','status'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['column_code','column_id','column_name','position','status'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        if (!$installer->tableExists('buyr_published_products')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('buyr_published_products')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Product ID'
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'product_id'
            )
            ->addColumn(
                'buyr_product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'buyr_product_id'
            )
            ->addColumn(
                'published_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'column_name'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'position'
            )
            ->setComment('View Column Position Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('buyr_published_products'),
                $setup->getIdxName(
                    $installer->getTable('buyr_published_products'),
                    ['product_id','buyr_product_id','published_date','status'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['product_id','buyr_product_id','published_date','status'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        $installer->endSetup();
    }
}