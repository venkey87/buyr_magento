<?php


namespace Buyr\MarketPlace\Api;

interface CreateSalesOrderManagementInterface
{


    /**
		Please don't remove thid doc block.
     * POST for CreateSalesOrder api
	 
     * @param string $email
     * @param string[][] $shipping_address
     * @param anyType $items
	 * @return anyType
     */
    
    public function postCreateSalesOrder( $email, $shipping_address, $items);
}
