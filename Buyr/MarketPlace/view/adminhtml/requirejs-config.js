/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
        	jqueryMin:'Buyr_MarketPlace/js/jquery.min',
        	jqueryLatestMin:'Buyr_MarketPlace/js/jquery-latest.min',
        	jqueryUi:'Buyr_MarketPlace/js/jquery-ui',
            jqueryValidateMin:'Buyr_MarketPlace/js/jquery.validate.min',
            additionalMethodsMin:'Buyr_MarketPlace/js/additional-methods.min',
            buyr:'Buyr_MarketPlace/js/buyr',
            tetherMin:'Buyr_MarketPlace/js/tether.min',
            bootstrapMin:'Buyr_MarketPlace/js/bootstrap.min',
            jqueryTablesorterMin:'Buyr_MarketPlace/js/jquery.tablesorter.min',
            jqueryTablesorterWidgetsMin:'Buyr_MarketPlace/js/jquery.tablesorter.widgets',
            jqueryDragtableMod:'Buyr_MarketPlace/js/jquery.dragtable.mod.min',
            mrhighcharts:'Buyr_MarketPlace/js/highcharts',
            highslidefull:'Buyr_MarketPlace/js/highslide-full.min'
        }
    }
};

(function() {
var config = {
    paths: {            
            'spsortElements': "Buyr_MarketPlace/js/jquery.sortElements"
        },   
    shim: {
        'spsortElements': {
            'deps': ['jquery','jquery/ui']
        }
    }
};
require.config(config);
})();

(function() {
var config = {
    paths: {            
            'spdrag': "Buyr_MarketPlace/js/jquery.dragtable"
        },   
    shim: {
        'spdrag': {
            'deps': ['jquery','jquery/ui']
        }
    }
};
require.config(config);
})();

(function() {
var config = {
    paths: {            
            'freshsliderMin':'Buyr_MarketPlace/js/freshslider.min'
        },   
    shim: {
        'freshsliderMin': {
            'deps': ['jquery','jquery/ui']
        }
    }
};
require.config(config);
})();

(function() {
var config = {
    paths: {            
            'jquerySlimscrollMin':'Buyr_MarketPlace/js/jquery.slimscroll.min'
        },   
    shim: {
        'jquerySlimscrollMin': {
            'deps': ['jquery','jquery/ui']
        }
    }
};
require.config(config);
})();


(function() {
var config = {
    paths: {            
            'sptablesorter': "Buyr_MarketPlace/js/jquery.tablesorter.min"
        },   
    shim: {
        'sptablesorter': {
            'deps': ['jquery']
        }
    }
};
require.config(config);
})();