    require(['jquery'],function($){

        jQuery(document).ready(function(){
        	move_to_first=function(){
				jQuery('#page_no').val(1);
				jQuery('.numbers_count').removeClass("active");
				jQuery('.first2').addClass('active');
				jQuery('.form').each(function(){
					jQuery(this).addClass('hide');
				});
				jQuery('#form1').removeClass('hide');
				
			}
			move_to_second=function(){
				jQuery('#page_no').val(2);
				jQuery('.numbers_count').removeClass("active");
				jQuery('.second2').addClass('active');
				jQuery('.form').each(function(){
					jQuery(this).addClass('hide');
				});
				jQuery('#form2').removeClass('hide');	
			}

			move_to_third=function(){
				jQuery('#page_no').val(3);
				jQuery('.numbers_count').removeClass("active");
				jQuery('.third2').addClass('active');
				jQuery('.form').each(function(){
					jQuery(this).addClass('hide');
				});
				jQuery('#form3').removeClass('hide');
				
			}

			move_to_fourth=function(){
				jQuery('#page_no').val(4);
				jQuery('.numbers_count').removeClass("active");
				jQuery('.fourth2').addClass('active');
				jQuery('.form').each(function(){
					jQuery(this).addClass('hide');
				});
				jQuery('#form4').removeClass('hide');
				
			}
        });

    	jQuery('#business_phone').keyup(function(){
			var phonenoval = jQuery(this).val();
			jQuery('#adminPhone').val(phonenoval);
		});

		jQuery(".term_condition").click(function(){
           jQuery(".term_condition_details").toggle();
	    });

    	jQuery(document).on('keyup','input[type="text"]',function(){

    			var thisid = jQuery(this).attr('id');
    			//alert(thisid);
    			/**/
    			if(thisid == "cvv")
    			{
                     var fieldvalue = jQuery(this).val();
                     if(fieldvalue != "" && fieldvalue.length == 3)
                     {
                        jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
                     }
                     else
                     {
                     	if(fieldvalue == "")
                     	{
							jQuery(this).addClass('error');
							jQuery(this).removeClass('valid');
							jQuery(this).parent().find('.error').show();
							jQuery(this).parent().find('.error').text("This field is required");
					    }
					    else
					    {
							jQuery(this).addClass('error');
							jQuery(this).removeClass('valid');
							jQuery(this).parent().find('.error').show();
							jQuery(this).parent().find('.error').text("This field is required");

					    }
                     }
    			}
    			if(thisid == "expiration_date_month")
    			{
                     var fieldvalue = jQuery(this).val();
                     if(fieldvalue != "" && fieldvalue > 0 && fieldvalue < 13 && fieldvalue.length == 2)
                     {
                        jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
                     }
                     else
                     {
                     	if(fieldvalue == "")
                     	{
	                        jQuery(this).addClass('error');
							jQuery(this).removeClass('valid');
							jQuery(this).parent().find('.error').show();
							jQuery(this).parent().find('.error').text("This field is required");
					    }
					    else
					    {
					    	jQuery(this).addClass('error');
							jQuery(this).removeClass('valid');
							jQuery(this).parent().find('.error').show();
							jQuery(this).parent().find('.error').text("Value should be between 01 and 12");
					    }
                     }
    			}
    			if(thisid == "expiration_date_year")
    			{
                     var fieldvalue = jQuery(this).val();
                     if(fieldvalue != "" && fieldvalue > 2016 && fieldvalue.length == 4)
                     {
                        jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
                     }
                     else
                     {
                     	if(fieldvalue == "")
                     	{
							jQuery(this).addClass('error');
							jQuery(this).removeClass('valid');
							jQuery(this).parent().find('.error').show();
							jQuery(this).parent().find('.error').text("This field is required");
					    }
					    else
					    {
							jQuery(this).addClass('error');
							jQuery(this).removeClass('valid');
							jQuery(this).parent().find('.error').show();
							jQuery(this).parent().find('.error').text("Year should be started from 2017");
					    }
                     }
    			}
				if(thisid == "business_phone" || thisid == "adminPhone")
				{
					//alert();
					var fieldvalue = jQuery(this).val();
					var intRegex = /[0-9 -()+]+$/;
					var speExpression = /[`~'"_=+\\!@#$%\^&*(){}[\]<>?/|\- ]/;
					if(intRegex.test(fieldvalue) && fieldvalue != "" && fieldvalue.length == 10 && !speExpression.test(fieldvalue) && fieldvalue.indexOf('.') == -1 && jQuery.isNumeric(fieldvalue)) {
						jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
					}
					else
					{
						if(fieldvalue == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

						}
						else
						{
							jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Please specify a valid phone number");
					    }
					}

				}
				if(thisid == "card_number" || thisid == "account_number")
				{
					var fieldvalue = jQuery(this).val();
					var intRegex = /[0-9 -()+]+$/;
					var speExpression = /[`~'"_=+\\!@#$%\^&*(){}[\]<>?/|\- ]/;
					if(intRegex.test(fieldvalue) && fieldvalue != "" && !speExpression.test(fieldvalue) && fieldvalue.indexOf('.') == -1 && jQuery.isNumeric(fieldvalue)) {
						jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
					}
					else
					{
						if(fieldvalue == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

						}
						else
						{
							jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Please enter a valid number.");

					    }
					}

				}

				if(thisid == "zip" || thisid == "product_origin_zip")
				{
					var fieldvalue = jQuery(this).val();
					var speExpression = /[`~'"_=+\\!@#$%\^&*(){}[\]<>?/|\- ]/;
					if(jQuery.isNumeric(fieldvalue) == true && fieldvalue.length == 5 && !speExpression.test(fieldvalue) && fieldvalue != "" && fieldvalue.indexOf('.') == -1){
						jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
					}
					else
					{
						if(fieldvalue == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

						}
						else
						{
							jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("The specified US ZIP Code is invalid");
					    }
					}
				}

				if(thisid == "city" || thisid == "state" || thisid == "product_origin_city" || thisid == "product_origin_state" || thisid == "name_on_card")
				{
                     var fieldvalue = jQuery(this).val();
                     var letters = /^[A-Za-z]+$/;  
                     if(fieldvalue.match(letters) && fieldvalue != "")  
                     {
                        jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
                     }
                     else
                     {
                     	if(fieldvalue == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

						}
						else
						{
	                     	jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Letters only please");
					    }
                     }
				}
				if(thisid == "name_on_card")
				{
                     var fieldvalue = jQuery(this).val();
                     var letters = /^[A-Za-z ]+$/;  
                     if(fieldvalue.match(letters) && fieldvalue != "")  
                     {
                        jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
                     }
                     else
                     {
                     	if(fieldvalue == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

						}
						else
						{
	                     	jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Letters only please");
					    }
                     }
				}
				if(thisid == "firstname" || thisid == "lastname")
				{
                     var fieldvalue = jQuery(this).val();
                     var letters = /^[A-Za-z]+$/;  
                     if(fieldvalue.match(letters) && fieldvalue != "")  
                     {
                        jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
                     }
                     else
                     {
                     	if(fieldvalue == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

						}
						else
						{
	                     	jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Letters only please");

					    }
                     }
				}

				if(thisid == "entity_name" || thisid == "trade_name" || thisid == "address_line_1" || thisid == "product_origin_address_line_1" || thisid == "routing_number"){
					if(jQuery(this).val() == ''){
					jQuery(this).addClass('error');
					jQuery(this).removeClass('valid');
					jQuery(this).parent().find('.error').show();
					jQuery(this).parent().find('.error').text("This field is required");
					}else{

						jQuery(this).addClass('valid');
						jQuery(this).removeClass('error');
						jQuery(this).parent().find('.error').hide();
						
					}
				}
				if(thisid == "ein_number"){
					if(jQuery(this).val() != '' && jQuery(this).val().length == 9){
					    jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
					}else{

						if(jQuery(this).val() == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");
					    }
					    else
					    {
					    	jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Please enter a correct EIN no.");
					    }
						
					}
				}

				if(thisid == "email")
    			{
    			   var fieldvalue = jQuery(this).val();
                   var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                   if(pattern.test(fieldvalue) && fieldvalue != "")
                   {
                        jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
                   }
                   else
				    {
						if(fieldvalue == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

						}
						else
						{
							jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Please specify a valid email id");
					    }
					}
    			}
    	});
        
        jQuery(document).on('blur','input[type="password"]',function(){
        	var thisid = jQuery(this).attr('id');

        	var password = jQuery('#password').val();
			var cpassword = jQuery('#confirmpassword').val();
			if(cpassword == password && cpassword != "")
			{
				jQuery('#confirmpassword').addClass('valid');
				jQuery('#confirmpassword').removeClass('error');
				jQuery('#confirmpassword').parent().find('.error').hide();

			}
			else
			{
				jQuery('#confirmpassword').addClass('error');
				jQuery('#confirmpassword').removeClass('valid');
				jQuery('#confirmpassword').parent().find('.error').show();
				jQuery('#confirmpassword').parent().find('.error').text("Must match the previous entry");
			}


        });

        jQuery(document).on('blur','#business_phone',function(){

        	var fieldvalue = jQuery('#adminPhone').val();
			var intRegex = /[0-9 -()+]+$/;
			var speExpression = /[`~'"_=+\\!@#$%\^&*(){}[\]<>?/|\- ]/;
			if(intRegex.test(fieldvalue) && fieldvalue != "" && fieldvalue.length == 10 && !speExpression.test(fieldvalue) && fieldvalue.indexOf('.') == -1 && jQuery.isNumeric(fieldvalue)) {
				jQuery('#adminPhone').addClass('valid');
			    jQuery('#adminPhone').removeClass('error');
			    jQuery('#adminPhone').parent().find('.error').hide();
			}
			else
			{
				if(fieldvalue == "")
				{
                    jQuery('#adminPhone').addClass('error');
				    jQuery('#adminPhone').removeClass('valid');
				    jQuery('#adminPhone').parent().find('.error').show();
				    jQuery('#adminPhone').parent().find('.error').text("This field is required");

				}
				else
				{
					jQuery('#adminPhone').addClass('error');
				    jQuery('#adminPhone').removeClass('valid');
				    jQuery('#adminPhone').parent().find('.error').show();
				    jQuery('#adminPhone').parent().find('.error').text("Please specify a valid phone number");
			    }
			}
        });

    	jQuery(document).on('keyup','input[type="password"]',function(){

    		var thisid = jQuery(this).attr('id');

    		if(thisid == "password")
    		{
    				var fieldvalue = jQuery(this).val();
    				if(fieldvalue != "" && fieldvalue.length >= 6)
    				{
                        jQuery(this).addClass('valid');
					    jQuery(this).removeClass('error');
					    jQuery(this).parent().find('.error').hide();
    				}
    				else
    				{
    					if(fieldvalue == "")
						{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

						}
						else
						{
							jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Please enter atleast 6 characters");
					    }

    				}
    		}
    		if(thisid == "confirmpassword")
    		{
    			var password = jQuery('#password').val();
    			var cpassword = jQuery(this).val();
    			if(cpassword == password && cpassword != "")
    			{
    				jQuery(this).addClass('valid');
					jQuery(this).removeClass('error');
					jQuery(this).parent().find('.error').hide();

    			}
    			else
    			{
    				if(fieldvalue == "")
					{
                            jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("This field is required");

					}
					else
					{
							jQuery(this).addClass('error');
						    jQuery(this).removeClass('valid');
						    jQuery(this).parent().find('.error').show();
						    jQuery(this).parent().find('.error').text("Must match the previous entry");
					}

    			}

    		}
    	});
        
        jQuery(document).on('click','.submit1',function(){
		    if(jQuery('#firstname').hasClass('valid') && jQuery('#lastname').hasClass('valid') && jQuery('#email').hasClass('valid') && jQuery('#password').hasClass('valid') && jQuery('#confirmpassword').hasClass('valid') && jQuery('#terms_condition').prop('checked'))
		    {

		       //jQuery(this).prop("disabled", false); 
		       jQuery('.first').hide();
		       jQuery('.first2').show();
               move_to_second();
               jQuery('.second').addClass('active');
               
		    }
		    else
		    {
		    	jQuery("#form11 :input").each(function(){
				    if(jQuery(this).val() == "")
				    {
				    	jQuery(this).removeClass('error');
				    	jQuery(this).addClass('error');
				    	jQuery(this).parent().find('.error').text("This field is required");

				    }
				})
		    }
	    });

        jQuery(document).on('click','.submit2',function(){
		    if(jQuery('#entity_name').hasClass('valid') && jQuery('#trade_name').hasClass('valid') && jQuery('#ein_number').hasClass('valid') && jQuery('#business_phone').hasClass('valid') && jQuery('#adminPhone').hasClass('valid') && jQuery('#address_line_1').hasClass('valid') && jQuery('#city').hasClass('valid') && jQuery('#zip').hasClass('valid') && jQuery('#product_origin_address_line_1').hasClass('valid') && jQuery('#product_origin_city').hasClass('valid') && jQuery('#product_origin_zip').hasClass('valid'))
		    {
		       //jQuery(this).prop("disabled", false); 
		       jQuery('.second').hide();
		       jQuery('.second2').show();
               move_to_third();
               jQuery('.third').addClass('active');
		    }
		    else
		    {
		    	jQuery("#form21 :input,#form22 :input,#form23 :input").each(function(){
				    if(jQuery(this).val() == "")
				    {
				    	if(jQuery(this).attr('id') != "address_line_2" && jQuery(this).attr('id') != "product_origin_address_line_2")
				    	{
				    	jQuery(this).removeClass('error');
				    	jQuery(this).addClass('error');
				    	jQuery(this).parent().find('.error').text("This field is required");
				        }

				    }
				})
		    }
	    });

        jQuery(document).on('click','.submit3',function(){
            if(jQuery('.radio').hasClass('valid'))
            {
		       jQuery('.third').hide();
		       jQuery('.third2').show();
               move_to_fourth();
               jQuery('.fourth').addClass('active');
            }

	    });

        jQuery(document).on('click','.submit4',function(){
		    if(jQuery('#routing_number').hasClass('valid') && jQuery('#account_number').hasClass('valid') && jQuery('#name_on_card').hasClass('valid') && jQuery('#card_number').hasClass('valid') && jQuery('#cvv').hasClass('valid') && jQuery('#expiration_date_month').hasClass('valid') && jQuery('#expiration_date_year').hasClass('valid'))
		    {
		       jQuery("#signupForm").submit();
		    }
		    else
		    {
		    	jQuery("#form41 :input,#form42 :input").each(function(){
				    if(jQuery(this).val() == "")
				    {
				    	jQuery(this).removeClass('error');
				    	jQuery(this).addClass('error');
				    	jQuery(this).parent().find('.error').text("This field is required");

				    }
				})
		    }
	    });

        jQuery(document).on('keyup','#form11 :input',function(){
				setTimeout(function(){
					if(jQuery('#firstname').hasClass('valid') && jQuery('#lastname').hasClass('valid') && jQuery('#email').hasClass('valid') && jQuery('#password').hasClass('valid') && jQuery('#confirmpassword').hasClass('valid') && jQuery('#terms_condition').prop('checked'))
			        {
					    jQuery(".submit1").removeClass("disable_btn");
					     //jQuery(".submit1").prop("disabled", false); 
				    }
				    else
				    {
				    	jQuery(".submit1").addClass("disable_btn");
				    	 //jQuery(".submit1").prop("disabled", true); 
				    }
			    }  , 1000 );
	    });

        jQuery(document).on('click','#terms_condition',function(){
		    if(jQuery("#terms_condition").is(':checked') == true){
		    	if(jQuery('#firstname').hasClass('valid') == true && jQuery('#lastname').hasClass('valid') == true && jQuery('#email').hasClass('valid') == true && jQuery('#password').hasClass('valid') == true && jQuery('#confirmpassword').hasClass('valid') == true)
				{
						    jQuery(".submit1").removeClass("disable_btn");
						    //jQuery(".submit1").prop("disabled", false); 
			    }
			    else
			    {
			    	jQuery(".submit1").addClass("disable_btn");
			    	//jQuery(".submit1").prop("disabled", true); 
			    }
			    
			}
			else
			{
						 jQuery(".submit1").addClass("disable_btn");   
			}
        });

        jQuery(document).on('keyup','#form21 :input,#form22 :input,#form23 :input',function(){
            setTimeout(function(){
				if(jQuery('#entity_name').hasClass('valid') && jQuery('#trade_name').hasClass('valid') && jQuery('#ein_number').hasClass('valid') && jQuery('#business_phone').hasClass('valid') && jQuery('#adminPhone').hasClass('valid') && jQuery('#address_line_1').hasClass('valid') && jQuery('#city').hasClass('valid') && jQuery('#zip').hasClass('valid') && jQuery('#product_origin_address_line_1').hasClass('valid') && jQuery('#product_origin_city').hasClass('valid') && jQuery('#product_origin_zip').hasClass('valid'))
		        {
				    jQuery(".submit2").removeClass("disable_btn");
				    //jQuery(".submit2").prop("disabled", false); 
			    }
			    else
			    {
			    	jQuery(".submit2").addClass("disable_btn");
			    	//jQuery(".submit2").prop("disabled", true); 
			    }
			}  , 1000 );
	    });

        jQuery(document).on('keyup','#form41 :input,#form42 :input',function(){
		    setTimeout(function(){
				if(jQuery('#routing_number').hasClass('valid') && jQuery('#account_number').hasClass('valid') && jQuery('#name_on_card').hasClass('valid') && jQuery('#card_number').hasClass('valid') && jQuery('#cvv').hasClass('valid') && jQuery('#expiration_date_month').hasClass('valid') && jQuery('#expiration_date_year').hasClass('valid'))
		        {
				    jQuery(".submit4").removeClass("disable_btn");
				    //jQuery(".submit4").prop("disabled", false); 
			    }
			    else
			    {
			    	jQuery(".submit4").addClass("disable_btn");
			    	//jQuery(".submit4").prop("disabled", true); 
			    }
			}  , 1000 );
	    });
    });