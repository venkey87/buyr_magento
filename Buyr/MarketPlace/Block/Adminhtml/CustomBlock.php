<?php
namespace Buyr\MarketPlace\Block\Adminhtml;

use Magento\Backend\Block\Template;
//use Buyr\MarketPlace\Helper\ConfigHelper;

class CustomBlock extends Template
{
    public function getProductCategories($productid)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
        $categories = $_product->getCategoryIds();
        $categorylist = "";
        foreach($categories as $category){
            $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
            $categorylist = $categorylist.",".$cat->getName();
        }
        $categorylist = ltrim($categorylist,',');
        return $categorylist;
    }
    public function getMaxPrice()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection(); 
        $maxproductprice = "SELECT max(price) as max_price ,min(price)as min_price FROM catalog_product_index_price ORDER BY customer_group_id";   
        $rows = $connection->fetchAll($maxproductprice); 
        foreach($rows as $rows_new)
        {
            $max_price = $rows_new['max_price'];
        }
        return sprintf("%.2f",$max_price);
    }
    public function getMinPrice()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection(); 
        $maxproductprice = "SELECT max(price) as max_price ,min(price) as min_price FROM catalog_product_index_price ORDER BY customer_group_id";   
        $rows = $connection->fetchAll($maxproductprice); 
        foreach($rows as $rows_new)
        {
            $min_price = $rows_new['min_price'];  
        }
        return sprintf("%.2f",$min_price);
    }
    public function getMaxQty()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection(); 
        $maxqty = "select max(qty) as max_qty from cataloginventory_stock_item";
        $rows = $connection->fetchAll($maxqty); 
        foreach($rows as $rows_new)
        {
            $max_qty = $rows_new['max_qty'];
        }

        return round($max_qty+1);

    }
    public function getMinQty()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection(); 
        $minqty = "select min(qty) as min_qty from cataloginventory_stock_item";
        $rows = $connection->fetchAll($minqty); 
        foreach($rows as $rows_new)
        {
            $min_qty = $rows_new['min_qty'];
        }

        return round($min_qty);

    }

    public function getAttributeSets()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $attributesets = "select attribute_set_id as id, attribute_set_name as name from eav_attribute_set where entity_type_id=4";
        $rows = $connection->fetchAll($attributesets);
        $attributesetlist = array();
        foreach($rows as $rows_new)
        {
           $attributesetlist[$rows_new['id']] = $rows_new['name'];
        }

        return $attributesetlist;

    }
    public function getAllAttributes()
    {
        $attributesorg = array('color','cost','country_of_manufacture','custom_design_from','custom_design_to','manufacturer','meta_description','meta_keyword','meta_title','minimal_price','msrp','short_description','size','special_price','tax_class_id','url_key','visibility','weight');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $productattributes = "select attribute_code as code from eav_attribute where entity_type_id=4";
        $rows = $connection->fetchAll($productattributes);
        $attributes = array();
        foreach ($rows as $rows_new) {
            if(in_array($rows_new['code'], $attributesorg))
            {
              $attributes[] = $rows_new['code'];
            }
        }
        return $attributes;
    }
    public function getAttributeName($code)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $attribute_info = $objectManager->create('Magento\Eav\Model\Entity\Attribute')->loadByCode('catalog_product', $code);
       
        return $attribute_info->getFrontendLabel();
    }
    public function getAttributeColumns()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $attributecolumn = "select column_code as code from buyr_attribute_column where status=1";
        $rows = $connection->fetchAll($attributecolumn);
        $columnname = array();
        foreach ($rows as $rows_new) {
              $columnname[] = $rows_new['code'];
        }

        return $columnname;

    }
    public function getAttributeValue($productid,$attricutecode)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
        $value = $product->getData($attricutecode);
        $attr = $product->getResource()->getAttribute($attricutecode);
        if ($attr->usesSource()) {
               $value = $attr->getSource()->getOptionText($value);
        }

        return $value;

    }
    public function getAllViews()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $allviewsquery = "select * from buyr_seller_view";
        $rows = $connection->fetchAll($allviewsquery);
        $allview = array();
        foreach ($rows as $value) {
            $link = 'buyr/productmanagement/index/idstarting/'.$value['idstarting'].'/idending/'.$value['idending'].'/pricestarting/'.$value['pricestarting'].'/priceending/'.$value['priceending'].'/qtystarting/'.$value['qtystarting'].'/qtyending/'.$value['qtyending'].'/visibility/'.$value['visibility'].'/attributeset/'.$value['attributeset'].'/skufilter/'.$value['sku'].'/productstatus/'.$value['visibilitystatus'].'/columnorder/'.$value['col_pos_id'].'/viewname/'.$value['view_name'];
            
            $allview[$value['view_name'].','.$value['id']] = $link;
        }
        return $allview;

    }
    public function productStatus($productid)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $checkstatusquery = "select status as productstatus from buyr_published_products where product_id='".$productid."'";
        $rows = $connection->fetchAll($checkstatusquery);
        $result = 0;
        foreach ($rows as $value) {
            if($value['productstatus'] == 1)
            {
                $result = 1;
            }
            else
            {
                $result = 0;
            }

        }

        return $result;
    }
    public function getColumnOrganization($columnorder)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $selectColumnserialQuery = "select * from buyr_view_column_position where id='".$columnorder."'";
        $rows = $connection->fetchAll($selectColumnserialQuery);
        foreach ($rows as $rows_new) {
            $result = $rows_new['column_names'];
            $result = ltrim($result,",");
            $result = explode(",",$result);
        }
        
        return $result;
    }
    public function getAttributeColumnsNames()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $attributecolumn = "select column_code as code from buyr_attribute_column";
        $rows = $connection->fetchAll($attributecolumn);
        $columnname = array();
        foreach ($rows as $rows_new) {
            $columnname[$this->getAttributeName($rows_new['code'])] = $rows_new['code'];
        }

        return $columnname;

    }
    public function getProductPrice($productid)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
        $product_type = $_product->getTypeId();
        if($product_type == "simple")
        {
            $price = $_product->getPrice();
        }
        else
        {
            $price = $_product->getFinalPrice();
        }
        return $price;
    }
    public function getPricingModelDetails($productid)
    {       
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

        $session = $objectManager->get('Magento\Customer\Model\Session');
        $authentication_key = $session->getAuthenticationKey();

        $connection = $resource->getConnection();
        $query_product_buyr_id = "select buyr_product_id as buyr_product_id from buyr_published_products where 	product_id='".$productid."'";
        $rows = $connection->fetchAll($query_product_buyr_id);
        $rowcount = count($rows);
        
        if($rowcount != 0)
        {
            foreach($rows as $rows_new)
            {
                 $buyr_product_id = $rows_new['buyr_product_id'];
            }

            $ApiUrls = $this->getApiUrls();
            
            $ch = curl_init($ApiUrls['pricingModelConfig'].'/'.$buyr_product_id);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                                                                                
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',
                'authorization:'.$authentication_key)                                                                       
            );                                                                                                                   
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
            $result = curl_exec($ch);
            $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $httpcode_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $result = json_decode($result,true);
            if($httpcode_status == 200)
            {
              $response = $result;
            }
            else
            {
              $response = $this->getDefaultPricingModelDetails($productid);
            }
        }
        else
        {
            $response = $this->getDefaultPricingModelDetails($productid);
        }

        return $response;

    }
    public function getDefaultPricingModelDetails($productid)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
        $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);

        $msrp = sprintf("%.2f",$_product->getMsrp());
        $map_price = sprintf("%.2f",$_product->getFinalPrice());
        $cost = sprintf("%.2f",$_product->getCost());
        $units = $StockState->getStockQty($_product->getId(), $_product->getStore()->getWebsiteId());
        $priceModel = "Constant";
        $startingPrice = sprintf("%.2f",$_product->getFinalPrice());
        $timeframeType = "Reset";
        $timeframePeriod = "Month";
        $priceChange = sprintf("%.2f",0.00);

        $response = Array(
                    "id" => "",
                    "productId" => "",
                    "priceModel" => $priceModel,
                    "mapPrice" => $map_price,
                    "units" => $units,
                    "msrp" => $msrp,
                    "cost" => $cost,
                    "startingPrice" => $startingPrice,
                    "priceChange" => $priceChange,
                    "timeframeType" => $timeframeType,
                    "timeframePeriod" => $timeframePeriod,
                    "timeframeValue" => "1",
                    "state" => "",
                    "model" => Array()
                    );

        return $response;

    }
    public function getProductState($productid)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

        $session = $objectManager->get('Magento\Customer\Model\Session');
        $authentication_key = $session->getAuthenticationKey();

        $connection = $resource->getConnection();
        $query_product_buyr_id = "select buyr_product_id as buyr_product_id from buyr_published_products where 	product_id='".$productid."'";
        $rows = $connection->fetchAll($query_product_buyr_id);
        $rowcount = count($rows);

        if($rowcount != 0)
        {
            foreach($rows as $rows_new)
            {
                 $buyr_product_id = $rows_new['buyr_product_id'];
            }

            $ApiUrls = $this->getApiUrls();
            
            $ch = curl_init($ApiUrls['pricingModelConfig'].'/'.$buyr_product_id.'/state');                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                                                                                
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',
                'authorization:'.$authentication_key)                                                                       
            );                                                                                                                   
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
            $result = curl_exec($ch);
            $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $httpcode_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $result = json_decode($result,true);
            if($httpcode_status == 200)
            {
              $response = $result;
            }
            else
            {
              $response = array("state"=>"disabled");
            }

        }
        else
        {
            $response = array("state"=>"disabled");
        }

        return $response;
        
    }
    public function getBuyrProductId($productid)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query_product_buyr_id = "select buyr_product_id as buyr_product_id from buyr_published_products where 	product_id='".$productid."'";
        $rows = $connection->fetchAll($query_product_buyr_id);
        $rowcount = count($rows);
        if($rowcount != 0)
        {
           foreach($rows as $rows_new)
           {
                 $buyr_product_id = $rows_new['buyr_product_id'];
           }

           $response = $buyr_product_id;
        }
        else
        {
           $response = "empty";
        }

        return $response;

    }
    public function getProductOffers($product_id_array)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $session = $objectManager->get('Magento\Customer\Model\Session');
        $authentication_key = $session->getAuthenticationKey();

        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $ids = join("','",$product_id_array);


        $query_product_buyr_id = "SELECT * FROM buyr_published_products WHERE product_id IN ('$ids')";
        $rows = $connection->fetchAll($query_product_buyr_id);
        $buyr_product_ids = array();
        foreach($rows as $rows_new)
        {
            $buyr_product_ids[] = $rows_new['buyr_product_id'];
        }

        $buyr_product_ids_json = json_encode($buyr_product_ids);

        $data_string = 
        '{
            "products": '.$buyr_product_ids_json.'
        }';
                                                                                                          
        $ApiUrls = $this->getApiUrls();

        $ch = curl_init($ApiUrls['offerCount']);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string),
            'authorization:'.$authentication_key)                                                                       
        );                                                                                                                   
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                   
        $result = curl_exec($ch);
        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        $response_array = json_decode($result,true);
        return $response_array;
    }

    public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }

    public function getCategoryIdwithName($categoryname)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query_category_id = "select entity_id as category_id from catalog_category_entity_varchar where value='".$categoryname."'";
        $rows = $connection->fetchAll($query_category_id);
        $rowcount = count($rows);
        if($rowcount != 0)
        {
            foreach($rows as $rows_new)
            {
                $cat_id = $rows_new['category_id'];
            }
        }
        else
        {
            $cat_id = "";
        }

        return $cat_id;

    }
    public function getManufacturerOptionId($optionLabel)
    {
        $attributeCode = 'manufacturer';
        $entityType = 'catalog_product';
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $attributeInfo = $objectManager->get(\Magento\Eav\Model\Entity\Attribute::class)->loadByCode($entityType, $attributeCode);
        $attributeId = $attributeInfo->getAttributeId();
        $attributeOptionAll = $objectManager->get(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
                                        ->setPositionOrder('asc')
                                        ->setAttributeFilter($attributeId)                                               
                                        ->setStoreFilter()
                                        ->load();
        $all_options = $attributeOptionAll->getData();
        
        $option_id = "";
        foreach($all_options as $all_options_new)
        {
            if(strtolower($all_options_new['value']) == strtolower($optionLabel)) { $option_id = $all_options_new['option_id']; }
        }

        return $option_id;
    }
    public function checkUpcExist($upcvalue)
    {
        if($upcvalue != "")
        {
            $entityType = 'catalog_product';
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $attributeInfo = $objectManager->get(\Magento\Eav\Model\Entity\Attribute::class)->loadByCode($entityType, "upc");
            $attributeId = $attributeInfo->getAttributeId();

            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $query_category_id = "select value as upc_value from catalog_product_entity_varchar where value LIKE '%".$upcvalue."%' and attribute_id=".$attributeId;
            $rows = $connection->fetchAll($query_category_id);
            $rowcount = count($rows);
            if($rowcount != 0)
            {
                foreach($rows as $rows_new)
                {

                if($rows_new != "") { $result = "found"; } else { $result = ""; }
                }
            }
            else
            {
                $result = "";
            }
        }
        else
        {
            $result = "";
        }
        

        return $result;

    }
}
