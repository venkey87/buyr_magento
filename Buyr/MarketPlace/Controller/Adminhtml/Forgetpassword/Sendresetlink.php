<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Forgetpassword;

class Sendresetlink extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
		if(isset($_REQUEST['email']))
		{
			$email = $_REQUEST['email'];
			$subject = "Reset Password";
			$resetpassurl = $this->getUrl('buyr/resetpassword/index');

			$message = "Click the link below to reset password <a href='".$resetpassurl."'>Click here</a>";

			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: <webmaster@example.com>' . "\r\n";
			$headers .= 'Cc: myboss@example.com' . "\r\n";

			/*if(mail($email,$subject,$message,$headers))
			{
				echo "success";
			}
			else
			{
				echo "fail";
			}*/

			$mail = mail($email,$subject,$message,$headers);

			echo $mail->ErrorInfo;

		    //echo "eeeeeee";
            //exit();
			
	    }
	}
}