<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Forgetpassword;

class Index extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
        return  $resultPage = $this->resultPageFactory->create();
	}


	/*
	 * Check permission via ACL resource
	 */
	protected function _isAllowed()
	{
		return $this->_authorization->isAllowed('Buyr_MarketPlace::post_manage');
	}

    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Buyr_MarketPlace::post');
        $resultPage->getConfig()->getTitle()->prepend((__('Posts')));

        $resultPage->addBreadcrumb(__('Buyr'), __('Buyr'));
        $resultPage->addBreadcrumb(__('Registration'), __('Registration'));

        return $this;
    }
}