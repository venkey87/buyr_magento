<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Profile;

class Updatebankinfo extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
        //return  $resultPage = $this->resultPageFactory->create();
        //$_REQUEST['routing_number'];
        //$_REQUEST['account_number'];
        //echo $_REQUEST['routing_number']."/".$_REQUEST['account_number'];
        //$authentication_token = $this->login();
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $object->get('Magento\Customer\Model\Session');
        if($session->getAuthenticationKey() != "")
        {
          $authentication_token = $session->getAuthenticationKey();
        }
        else
        {
          $authentication_token = "false";
        }
        
        if($authentication_token != "false")
        {
             echo $this->update($authentication_token);
        }

	}
	
	public function update($authentication_token)
	{
			$data_string = '{
			          "bankingInformation": {
			            "accountNumber": "'.$_REQUEST['account_number'].'",
			            "routingNumber": "'.$_REQUEST['routing_number'].'",
			            "verivication": true
			          }
			        }';   

					$ApiUrls = $this->getApiUrls();                                                        
			                                                                                                                             
			        $ch = curl_init($ApiUrls['profile']);                                                                      
			        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");                                                                     
			        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
			        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			            'Content-Type: application/json',                                                                                
			            'Content-Length: ' . strlen($data_string),
			            'authorization:'.$authentication_token)                                                                       
			        );                                                                                                                   
			        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
			        curl_setopt($ch, CURLOPT_HEADER, true);
			        $result = curl_exec($ch);

			        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			        curl_close($ch);

			        if($httpcode_patch == 200)
			        {
			        	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                        $connection = $resource->getConnection();
                        $tableName = $resource->getTableName('buyr_seller_info');
			        	
			        	$updatequery = "update buyr_seller_info set account_number='".$_REQUEST['account_number']."', routing_number='".$_REQUEST['routing_number']."'";
	                    $connection->query($updatequery);

	                    $status = "true";



			        }
			        else
			        {
			        	$status = "false";
			        }

			        return $status;
	}
	public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }
}