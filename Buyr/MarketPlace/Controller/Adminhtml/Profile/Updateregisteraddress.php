<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Profile;

class Updateregisteraddress extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
		$object = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $object->get('Magento\Customer\Model\Session');
        if($session->getAuthenticationKey() != "")
        {
          $authentication_token = $session->getAuthenticationKey();
        }
        else
        {
          $authentication_token = "false";
        }

        //$authentication_token = $this->login();
        if($authentication_token != "false")
        {
             echo $this->update($authentication_token);
        }

	}
	public function update($authentication_token)
	{
		    $profile_details = $this->getProfileInfo();

			$data_string = '{
			          "businessInfo": {
						"legalName": "'.$profile_details['entity_name'].'",
						"tradeName": "'.$profile_details['trade_name'].'",
						"officialAddress": {
						  "addressCountry": "USA",
						  "addressLocality": "'.$_REQUEST['address_line_2'].'",
						  "addressRegion": "'.$_REQUEST['state'].'",
						  "postOfficeBoxNumber": "string",
						  "postalCode": "'.$_REQUEST['zip'].'",
						  "addressCity": "'.$_REQUEST['city'].'",
						  "streetAddress": "'.$_REQUEST['address_line_1'].'"
						},
						"tradeAddress": {
						  "addressCountry": "USA",
						  "addressLocality": "'.$profile_details['product_origin_address_line_2'].'",
						  "addressRegion": "'.$profile_details['product_origin_state'].'",
						  "postOfficeBoxNumber": "string",
						  "postalCode": "'.$profile_details['product_origin_zip'].'",
						  "addressCity": "'.$profile_details['product_origin_city'].'",
						  "streetAddress": "'.$profile_details['product_origin_address_line_1'].'"
						},
						"verification": true,
						"businessPhone": "'.$profile_details['business_phone'].'",
						"adminPhone": "'.$profile_details['adminPhone'].'",
						"EIN_Number": "'.$profile_details['ein_number'].'"
					  }
					}'; 

					$ApiUrls = $this->getApiUrls();
                                                         
			                                                                                                                             
			        $ch = curl_init($ApiUrls['profile']);                                                                      
			        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");                                                                     
			        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
			        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			            'Content-Type: application/json',                                                                                
			            'Content-Length: ' . strlen($data_string),
			            'authorization:'.$authentication_token)                                                                       
			        );                                                                                                                   
			        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
			        curl_setopt($ch, CURLOPT_HEADER, true);
			        $result = curl_exec($ch);

			        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			        curl_close($ch);
                    
                    //$status = $result;

			        if($httpcode_patch == 200)
			        {
			        	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                        $connection = $resource->getConnection();
                        $tableName = $resource->getTableName('buyr_seller_info');
			        	
			        	$updatequery = "update buyr_seller_info set address_line_2='".$_REQUEST['address_line_2']."', state='".$_REQUEST['state']."', zip='".$_REQUEST['zip']."', city='".$_REQUEST['city']."', address_line_1='".$_REQUEST['address_line_1']."'";
	                    $connection->query($updatequery);

	                    $status = "true";

			        }
			        else
			        {
			        	$status = "false";
			        }

			        return $status;
	}
	public function getProfileInfo()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$getprofilequery = "select * from buyr_seller_info";
		$rows = $connection->fetchAll($getprofilequery);
		$profile_details = array();
		foreach ($rows as $rows_new) {
		    $profile_details['buyr_retailer_id'] = $rows_new['buyr_retailer_id'];
		    $profile_details['buyr_retailer_status'] = $rows_new['buyr_retailer_status'];
		    $profile_details['email'] = $rows_new['email'];
		    $profile_details['firstname'] = $rows_new['firstname'];
		    $profile_details['lastname'] = $rows_new['lastname'];
		    $profile_details['password'] = $rows_new['password'];
		    $profile_details['entity_name'] = $rows_new['entity_name'];
		    $profile_details['trade_name'] = $rows_new['trade_name'];
		    $profile_details['ein_number'] = $rows_new['ein_number'];
		    $profile_details['business_phone'] = $rows_new['business_phone'];
		    $profile_details['adminPhone'] = $rows_new['adminPhone'];
		    $profile_details['address_line_1'] = $rows_new['address_line_1'];
		    $profile_details['address_line_2'] = $rows_new['address_line_2'];
		    $profile_details['city'] = $rows_new['city'];
		    $profile_details['state'] = $rows_new['state'];
		    $profile_details['zip'] = $rows_new['zip'];
		    $profile_details['product_origin_address_line_1'] = $rows_new['product_origin_address_line_1'];
		     $profile_details['product_origin_address_line_2'] = $rows_new['product_origin_address_line_2'];
		    	
		    $profile_details['product_origin_city'] = $rows_new['product_origin_city'];
		    $profile_details['product_origin_state'] = $rows_new['product_origin_state'];
		    $profile_details['product_origin_zip'] = $rows_new['product_origin_zip'];
		    $profile_details['routing_number'] = $rows_new['routing_number'];
		    $profile_details['account_number'] = $rows_new['account_number'];
		    $profile_details['name_on_card'] = $rows_new['name_on_card'];
		    $profile_details['card_number'] = $rows_new['card_number'];
		    $profile_details['retailer_image'] = $rows_new['retailer_image'];
		    $profile_details['created_at'] = $rows_new['created_at'];

		}

		return $profile_details;
	}

	public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }
}