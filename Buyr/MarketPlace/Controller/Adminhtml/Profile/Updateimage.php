<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Profile;


class Updateimage extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	   $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
	   $baseurl = $storeManager->getStore()->getBaseUrl();
	   $baseurl = $baseurl.'image/';

       $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
       $target_dir  =  $directory->getRoot().'/image/';
		
		if(!is_dir($target_dir))
		{
            mkdir($target_dir);
		}

		$target_file = $target_dir . basename($_FILES["image"]["name"]);
		$filename = time().basename($_FILES["image"]["name"]);
		$filename = str_replace(" ","",$filename);
		$target_file = $target_dir.$filename;
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		$allowed =  array('gif','png','jpg');
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
       
        if(!in_array($ext,$allowed) ) {
		    echo 'error1';
		}
		else
		{
			if ($_FILES["image"]["size"] < 512900) {
				if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
				{
					$object = \Magento\Framework\App\ObjectManager::getInstance();
			        $session = $object->get('Magento\Customer\Model\Session');
			        if($session->getAuthenticationKey() != "")
			        {
			          $login = $session->getAuthenticationKey();
			        }
			        else
			        {
			          $login = "false";
			        }
                    
                    if($login != "false")
                    {
                    	$imagepath = $baseurl.$filename;
 
                    	$data = array($login,$imagepath);
                    	$sts = $this->update($data);
                    	
                    	if($sts != "false")
                    	{

						$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			            $connection = $resource->getConnection();
			            $tableName = $resource->getTableName('buyr_seller_info');
			        	
			        	$updatequery = "update buyr_seller_info set retailer_image='".$imagepath."'";
			            $connection->query($updatequery);

			            echo $imagepath;
			            }
		            }
				}
				else
				{
					echo "error3";
				}
			}
			else
			{
				echo "error2";
			}
	    }
		

	}
	public function update($authentication_token)
	{
			$data_string = '{
			          "image": "'.$authentication_token[1].'"}';  

			$ApiUrls = $this->getApiUrls();                                                           
			                                                                                                                             
			$ch = curl_init($ApiUrls['profile']);                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string),
				'authorization:'.$authentication_token[0])                                                                       
			);                                                                                                                   
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
			curl_setopt($ch, CURLOPT_HEADER, true);
			$result = curl_exec($ch);

			$httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);

			if($httpcode_patch == 200)
			{
				$status = "true";
			}
			else
			{
				$status = "false";
			}

			return $status;
	}

	public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }
}