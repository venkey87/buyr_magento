<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Register;
use Magento\Framework\Controller\ResultFactory;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;

class Submit extends \Magento\Backend\App\Action
{
	/**
	* User model factory
	*
	* @var \Magento\User\Model\UserFactory
	*/    
	protected $_userFactory;
	/**
     * RoleFactory
     *
     * @var roleFactory
     */
    private $roleFactory;
 
     /**
     * RulesFactory
     *
     * @var rulesFactory
     */
    private $rulesFactory;
    /**
     * Init
     *
     * @param \Magento\Authorization\Model\RoleFactory $roleFactory
     * @param \Magento\Authorization\Model\RulesFactory $rulesFactory
     */
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\User\Model\UserFactory $userFactory,
        \Magento\Authorization\Model\RoleFactory $roleFactory,
        \Magento\Authorization\Model\RulesFactory $rulesFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
		$this->_userFactory = $userFactory;
        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
	}

	public function execute()
	{ 
			   
			    if($_REQUEST['firstname'] != "" && $_REQUEST['lastname'] != "" && $_REQUEST['email'] != "" && $_REQUEST['password'] != "" && $_REQUEST['confirmpassword'] != "" && $_REQUEST['entity_name'] != "" && $_REQUEST['trade_name'] != "" && $_REQUEST['ein_number'] != "" && $_REQUEST['business_phone'] != "" && $_REQUEST['adminPhone'] != "" && $_REQUEST['address_line_1'] != "" && $_REQUEST['city'] != "" && $_REQUEST['state'] != "" && $_REQUEST['zip'] != "" && $_REQUEST['product_origin_address_line_1'] != "" && $_REQUEST['product_origin_city'] != "" && $_REQUEST['product_origin_state'] != "" && $_REQUEST['product_origin_zip'] != "" && $_REQUEST['routing_number'] != "" && $_REQUEST['account_number'] != "" && $_REQUEST['name_on_card'] != "" && $_REQUEST['card_number'] != "" && $_REQUEST['cvv'] != "" && $_REQUEST['expiration_date_month'] != "" && $_REQUEST['expiration_date_year'] != ""){
                      
					    
						

                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                        $connection = $resource->getConnection();
                        $tableName = $resource->getTableName('buyr_seller_info');

                        $truncatesql = "truncate table buyr_seller_info";
                        $connection->query($truncatesql);
                        $sql = "insert into buyr_seller_info set email='".$_REQUEST['email']."', firstname='".$_REQUEST['firstname']."', lastname='".$_REQUEST['lastname']."', password='".$_REQUEST['password']."', entity_name='".$_REQUEST['entity_name']."', trade_name='".$_REQUEST['trade_name']."', ein_number='".$_REQUEST['ein_number']."', business_phone='".$_REQUEST['business_phone']."', adminPhone='".$_REQUEST['adminPhone']."', address_line_1='".$_REQUEST['address_line_1']."', address_line_2='".$_REQUEST['address_line_2']."', city='".$_REQUEST['city']."', state='".$_REQUEST['state']."', zip='".$_REQUEST['zip']."', product_origin_address_line_1='".$_REQUEST['product_origin_address_line_1']."', product_origin_address_line_2='".$_REQUEST['product_origin_address_line_2']."', product_origin_city='".$_REQUEST['product_origin_city']."', product_origin_state='".$_REQUEST['product_origin_state']."', product_origin_zip='".$_REQUEST['product_origin_zip']."', routing_number='".$_REQUEST['routing_number']."', account_number='".$_REQUEST['account_number']."', name_on_card='".$_REQUEST['name_on_card']."', 	card_number='".$_REQUEST['card_number']."', subcription_model='".$_REQUEST['financial']."', retailer_image='', created_at=NOW()";

                        $connection->query($sql);

                        $email = $_REQUEST['email'];
                        $firstname = $_REQUEST['firstname'];
                        $lastname = $_REQUEST['lastname'];
                        $password = $_REQUEST['password'];

						
     
                        $data_string = '{
							  "first": "'.$firstname.'",
							  "last": "'.$lastname.'",
							  "email": "'.$email.'",
							  "password": "'.$password.'",
							  "retailer": true
							}
                        ';                                                                          
                        
						
                        $ApiUrls = $this->getApiUrls();

	                    $ch = curl_init($ApiUrls['signUp']);                                                                      
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                        'Content-Type: application/json',                                                                                
                        'Content-Length: ' . strlen($data_string))                                                                       
                        );                                                                                                                   
                        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
                        curl_setopt($ch, CURLOPT_HEADER, true);
                        $result = curl_exec($ch);


                        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        if($httpcode == 200)
                        {
	                        $datasignin = array("email" => $email, "password" => $password);                                                                   
	                        $data_string_signin = json_encode($datasignin); 

							$ApiUrls = $this->getApiUrls();                                                                                

	                        $ch_signin = curl_init($ApiUrls['signIn']);                                                                      
	                        curl_setopt($ch_signin, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	                        curl_setopt($ch_signin, CURLOPT_POSTFIELDS, $data_string_signin);                                                                  
	                        curl_setopt($ch_signin, CURLOPT_RETURNTRANSFER, true);                                                                      
	                        curl_setopt($ch_signin, CURLOPT_HTTPHEADER, array(                                                                          
	                        'Content-Type: application/json',                                                                                
	                        'Content-Length: ' . strlen($data_string_signin))                                                                       
	                        );                                                                                                                   
	                        curl_setopt($ch_signin,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
	                        curl_setopt($ch_signin, CURLOPT_HEADER, true);
	                        $result_signin = curl_exec($ch_signin);
                            $httpcode_signin = curl_getinfo($ch_signin, CURLINFO_HTTP_CODE);

                            $headerLen = curl_getinfo($ch_signin, CURLINFO_HEADER_SIZE);
							$curlBody = substr($result_signin, $headerLen);
							$body_obj = json_decode($curlBody,true);

                            if($httpcode_signin == 200)
                            {
		                        if (!curl_errno($ch_signin)) {
		                        	$headers=array();
		                        	$data_header=explode("\n",$result_signin);

		                        	$authorization = $data_header[3];
		                        	$authentication_token = trim(substr($authorization,14));

									$retailar_id = $body_obj['@id'];
									$seller_id_update_query = "update buyr_seller_info set buyr_retailer_id='".$retailar_id."'";
									$connection->query($seller_id_update_query);
								}


								curl_close($ch_signin);

		                        $data_string = '{
		                          "image": "",
		                          "businessInfo": {
		                            "legalName": "'.$_REQUEST['entity_name'].'",
		                            "tradeName": "'.$_REQUEST['trade_name'].'",
		                            "officialAddress": {
		                              "addressCity": "'.$_REQUEST['city'].'",
		                              "addressCountry": "USA",
		                              "addressLocality": "'.$_REQUEST['address_line_2'].'",
		                              "addressRegion": "'.$_REQUEST['state'].'",
		                              "postOfficeBoxNumber": "string",
		                              "postalCode": "'.$_REQUEST['zip'].'",
		                              "streetAddress": "'.$_REQUEST['address_line_1'].'"
		                            },
		                            "tradeAddress": {
		                              "addressCity": "'.$_REQUEST['product_origin_city'].'",
		                              "addressCountry": "USA",
		                              "addressLocality": "'.$_REQUEST['product_origin_address_line_2'].'",
		                              "addressRegion": "'.$_REQUEST['product_origin_state'].'",
		                              "postOfficeBoxNumber": "string",
		                              "postalCode": "'.$_REQUEST['product_origin_zip'].'",
		                              "streetAddress": "'.$_REQUEST['product_origin_address_line_1'].'"
		                            },
		                            "verification": true,
		                            "businessPhone": "'.$_REQUEST['business_phone'].'",
		                            "adminPhone": "'.$_REQUEST['adminPhone'].'",
		                            "EIN_Number": "'.$_REQUEST['ein_number'].'"
		                          },
		                          "creditInformation" : {
	                                "cardNumber" : "'.$_REQUEST['card_number'].'",
	                                "nameOnCard" : "'.$_REQUEST['name_on_card'].'",
	                                "verivication" : true
	                              },
		                          "bankingInformation": {
		                            "accountNumber": "'.$_REQUEST['account_number'].'",
		                            "routingNumber": "'.$_REQUEST['routing_number'].'",
		                            "verivication": true
		                          },
		                          "adminApproved": true
		                        }';

								$ApiUrls = $this->getApiUrls(); 

		                                                                                                                                             
		                        $ch = curl_init($ApiUrls['profile']);                                                                      
		                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");                                                                     
		                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
		                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		                            'Content-Type: application/json',                                                                                
		                            'Content-Length: ' . strlen($data_string),
		                            'authorization:'.$authentication_token)                                                                       
		                        );                                                                                                                   
		                        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
		                        curl_setopt($ch, CURLOPT_HEADER, true);
		                        $result = curl_exec($ch);

		                        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		                        curl_close($ch);

		                        if($httpcode_patch == 200)
		                        {
									$api_user_password = $this->generatePasswordForApiUser();
									$this->createMagentoApiUser($_REQUEST,$api_user_password);
									$this->sendApiUserCredential($firstname,$api_user_password,$authentication_token);

		                        	$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			                        $resultRedirect->setPath('buyr/register/index/status/1');
			                        return $resultRedirect;
		                        }
		                        else
		                        {
		                        	$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			                        $resultRedirect->setPath('buyr/register/index/status/1');
			                        return $resultRedirect;
		                        }
		                    }
		                    else
		                    {
		                    		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			                        $resultRedirect->setPath('buyr/register/index/status/3');
			                        return $resultRedirect;
		                    }

                        }

						       
                }
                else
                {

                        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                        $resultRedirect->setPath('buyr/register/index/status/0');
                        return $resultRedirect;
                }

	}

	public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }
	public function createMagentoApiUser($userinfo,$api_user_password)
	{	
        $role=$this->roleFactory->create();
        $role->setName($userinfo['firstname'])
                ->setPid(0)
                ->setRoleType(RoleGroup::ROLE_TYPE) 
                ->setUserType(UserContextInterface::USER_TYPE_ADMIN);
        $role->save(); 		
		

		$resource=['Magento_Backend::admin',
						'Magento_Sales::sales',
						'Magento_Sales::sales_operation',
						'Magento_Sales::sales_order',
						'Magento_Sales::actions',					
						'Magento_Sales::create',
						'Magento_Sales::actions_view',
						'Magento_Sales::email',
						'Magento_Sales::reorder',
						'Magento_Sales::actions_edit',
						'Magento_Sales::cancel',
						'Magento_Sales::review_payment',
						'Magento_Sales::capture',
						'Magento_Sales::invoice',
						'Magento_Sales::creditmemo',
						'Magento_Sales::hold',
						'Magento_Sales::unhold',
						'Magento_Sales::ship',
						'Magento_Sales::comment',
						'Magento_Sales::emails',
						'Magento_Sales::sales_invoice',
						'Magento_Sales::shipment',
						'Magento_Sales::sales_creditmemo',
						'Magento_Sales::transactions',
						'Magento_Catalog::catalog',
						'Magento_Catalog::catalog_inventory',
						'Magento_Catalog::products',
						'Magento_Catalog::categories'
						
					]; 
				  
		
        $this->rulesFactory->create()->setRoleId($role->getId())->setResources($resource)->saveRel();
		
		
		$adminInfo = [
						'username'  => $userinfo['firstname'],
						'firstname' => $userinfo['firstname'],
						'lastname'    => $userinfo['lastname'],
						'email'     => $userinfo['email'],
						'password'  => $api_user_password,       
						'interface_locale' => 'en_US',
						'is_active' => 1
						];

		$userModel = $this->_userFactory->create();
		$userModel->setData($adminInfo);

		$userModel->setRoleId($role->getId());
		try{
		   $userModel->save(); 
		   //echo "</br>User is created and saved</br>";
		} catch (\Exception $ex) {
		   //echo "In error section</br>";
		   //echo "erorr".$ex->getMessage();

		}

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
  
		$roleAuthModel = $objectManager->create('\Magento\Authorization\Model\Role');
		$userModel = $objectManager->create('\Magento\User\Model\User');

		$usermodel = $userModel->getCollection()->addFieldToFilter('is_active', 1);
		$id = $usermodel->getColumnValues('user_id');
		$name = $usermodel->getColumnValues('username');
		$userList = array_combine($id,$name);

		$rolemodel = $roleAuthModel->getCollection();
		$role_id = $rolemodel->getColumnValues('role_id');
		$role_name = $rolemodel->getColumnValues('role_name'); 
		$roleList = array_combine($role_id,$role_name);

	}
	public function generatePasswordForApiUser()
	{
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass);
	}
	public function sendApiUserCredential($key,$secret,$authentication_token)
	{
		$ApiUrls = $this->getApiUrls(); 

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$root = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $storeApiUrl  =  $root->getRoot().'/index.php/rest/V1/';

		$data_string = '{
							"connection":{
									"@type": "Magento",
									"key": "'.$key.'",
									"secret": "'.$secret.'",
									"url": "'.$storeApiUrl.'"
							}
						}
                        ';  
		                                                                                                                                             
		$ch = curl_init($ApiUrls['sendApiUser']);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string),
			'authorization:'.$authentication_token)                                                                       
		);                                                                                                                   
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
		curl_setopt($ch, CURLOPT_HEADER, true);
		$result = curl_exec($ch);

		$httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		//print_r($result);


	}
}