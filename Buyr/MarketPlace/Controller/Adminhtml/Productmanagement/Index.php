<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Productmanagement;

class Index extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query_authentication_key = "SELECT * FROM buyr_seller_info";
        $rows = $connection->fetchAll($query_authentication_key);
        $rowcount = count($rows);
        if($rowcount == 1)
        {
            foreach($rows as $rows_new)
            {
                $authentication_key = $rows_new['authentication_key'];
            }
            if($authentication_key != "")
            {
                $session = $objectManager->get('Magento\Customer\Model\Session');
                $session->unsAuthenticationKey();
                $session->setAuthenticationKey($authentication_key);
            }
        }

        return  $resultPage = $this->resultPageFactory->create();
	}
	protected function _isAllowed()
	{
		return $this->_authorization->isAllowed('Buyr_MarketPlace::post_manage');
	}

    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Buyr_MarketPlace::productmanagement');
        $resultPage->getConfig()->getTitle()->prepend((__('Productmanagement')));

        $resultPage->addBreadcrumb(__('Buyr'), __('Buyr'));
        $resultPage->addBreadcrumb(__('Productmanagement'), __('Productmanagement'));

        return $this;
    }
}