<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Productmanagement;
use Magento\Framework\Controller\ResultFactory;

class Pricingmodel extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
		$object = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $object->get('Magento\Customer\Model\Session');
        if($session->getAuthenticationKey() != "")
        {
          $login = $session->getAuthenticationKey();
        }
        else
        {
          $login = "false";
        }
		
        if($login != "false")
        {
        	$result = $this->getpricingmodel($login,$_REQUEST);
        	if($result != "false")
        	{
        		print_r($result);
        		
        	}
			else
			{        		
        		echo "unable to generate graph";        		
        	}   
        }  
		else
		{
				echo "false";
		}
	}
	
	public function getpricingmodel($token,$productinfo)
	{			
	 	$data_string = '{
	 	 					"cost":'.$productinfo['cost'].',
							"units":'.$productinfo['inventory'].',
							"startingPrice": '.$productinfo['startingprice'].',
							"priceChange": '.$productinfo['pricechange'].',
							"priceModel": "'.$productinfo['pricingmodel'].'"
						 }';
						 
	    if($productinfo['pricingmodel'] == "Constant")
		{
			$data_string = '{
								"cost":'.$productinfo['cost'].',
								"units":'.$productinfo['inventory'].',
								"startingPrice": '.$productinfo['startingprice'].',
								"priceModel": "'.$productinfo['pricingmodel'].'"
						    }';
		}

	    $ApiUrls = $this->getApiUrls();

	 	 
	 	$ch = curl_init($ApiUrls['pricingModelCompute']);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string),
				'authorization:'.$token)
				);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);

		$result = curl_exec($ch);
		$httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
	
	
		if($httpcode_patch == 200)
		{
			$result = $result;
		}
		else
		{
			$result = $result;
		}
		
		return $result;
	
	}

	public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }
	
}