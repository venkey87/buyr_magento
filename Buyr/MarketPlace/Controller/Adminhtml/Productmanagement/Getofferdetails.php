<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Productmanagement;
use Magento\Framework\Controller\ResultFactory;

class Getofferdetails extends \Magento\Backend\App\Action
{
  protected $resultPageFactory = false;
  public function __construct(
    \Magento\Backend\App\Action\Context $context,
    \Magento\Framework\View\Result\PageFactory $resultPageFactory
  ) {
    parent::__construct($context);
    $this->resultPageFactory = $resultPageFactory;
  }

  public function execute()
  {      
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $object->get('Magento\Customer\Model\Session');
        if($session->getAuthenticationKey() != "")
        {
          $login = $session->getAuthenticationKey();
        }
        else
        {
          $login = "false";
        }

        if($login != "false")
        {

            $result = $this->getOffer($login,$_REQUEST['buyrpid']);
            if($result != "false")
            {
               $msg = $result;
            }
            else
            {
               $msg = "false";
            }

            
        }  
        else
        {
            $msg = "Session expire";
        }
    
     
        print_r($msg);
  }

  public function getOffer($authentication_key,$buyr_product_id)
  {
            $ApiUrls = $this->getApiUrls();

            $ch = curl_init($ApiUrls['getOffer'].'/'.$buyr_product_id);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                                                                                
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',
                'authorization:'.$authentication_key)                                                                       
            );                                                                                                                   
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
            $result = curl_exec($ch);
            $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $httpcode_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $result = json_decode($result,true);

            if($httpcode_status == 200)
            {
              $response = array();
              foreach($result as $result_new)
              {
                  $d1 =  (isset($result_new['expirationDate'])) ? $result_new['expirationDate'] : date('d-m-Y h:i:s');
                  //$d2= date('d-m-Y h:i:s');
                  $d2 = gmdate(DATE_ATOM);
                  $datediff = (isset($result_new['expirationDate'])) ? round((strtotime($d1) - strtotime($d2))/3600, 1) : 0;

                  

                  if($result_new['fulfillment'] == "pickUpInStore") { $fulfillment = "Pickup"; } else if($result_new['fulfillment'] == "delivery") { $fulfillment = "Delivery"; }

                  //$deliveryaddress = array("@id"=>"058CD228-346B-4480-8495-90DB4BB3D289","zip" => "80234","city" => "Westminster","last" => "Second","first" => "First","street" => "631 W 123rd ave","default" => true,"apartment" => "Unit 8202");

                  $response['price'][] = $result_new['price'];
                  $response['time'][] = date('h a',strtotime($result_new['creationDate']));
                  $response['consumerId'][] = $result_new['consumerName'];
                  $response['fulfillment'][] = $fulfillment;
                  $response['datediff'][] = $datediff;
                  $response['deliveryAddress'][] = $result_new['deliveryAddress'];
                  $response['offerid'][] = $result_new['@id'];
                  $response['avatarURL'][] = $result_new['avatarURL'];
                  /*$response['state'][] = $result_new['@state'];
                  $response['consumerId'][] = $result_new['consumerId'];
                  $response['productId'][] = $result_new['productId'];
                  $response['priceModelId'][] = $result_new['priceModelId'];
                  $response['qty'][] = $result_new['qty'];
                  $response['price'][] = $result_new['price'];
                  $response['fulfillment'][] = $result_new['fulfillment'];
                  $response['deliveryAddress'][] = $result_new['deliveryAddress'];
                  $response['retailerId'][] = $result_new['retailerId'];
                  $response['originalId'][] = $result_new['originalId'];
                  $response['avatarURL'][] = $result_new['avatarURL'];*/

              }

            }
            else
            {
              $response = "false";
            }



            //print_r($response);
            echo json_encode($response);
  }
  public function getApiUrls()
  {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
  }
  
}