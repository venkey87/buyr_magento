<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Productmanagement;
use Magento\Framework\Controller\ResultFactory;

class Unpublishproduct extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{ 
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $object->get('Magento\Customer\Model\Session');
        if($session->getAuthenticationKey() != "")
        {
          $login = $session->getAuthenticationKey();
        }
        else
        {
          $login = "false";
        }
        
        if($login != "false")
        {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();
                $tableName = $resource->getTableName('buyr_published_products');

                $sql = "select * from buyr_published_products where product_id='".$_REQUEST['product_id']."'";

                $rows = $connection->fetchAll($sql); 
                foreach ($rows as $rows_new) {
                    $buyr_product_id = $rows_new['buyr_product_id'];
                }

                $result = $this->unpublish($login,$buyr_product_id);         
        }  
        echo $result;
	}

	public function unpublish($token,$buyrproductid)
	{
        $data_string = '{"@state": "disabled"}';
		                                                                                                  
		$ApiUrls = $this->getApiUrls();

        $ch = curl_init($ApiUrls['publishProduct'].'/'.$buyrproductid);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string),
            'authorization:'.$token)                                                                       
        );                                                                                                                   
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
        curl_setopt($ch, CURLOPT_HEADER, true);
        $result = curl_exec($ch);
        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        

        if($httpcode_patch == 200)
        {
             $this->disablePricingModel($buyrproductid,$token);

             $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
             $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
             $connection = $resource->getConnection();
             $tableName = $resource->getTableName('buyr_published_products');

             $updatequery = "update buyr_published_products set status=0 where buyr_product_id='".$buyrproductid."'";
             $connection->query($updatequery);

             $result = "success";
        }
        else
        {
        	$result = "false";
        }

        return $result;

	}


	public function getProductInfo($productid)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
        
        return $_product;
	}

    public function disablePricingModel($buyrproductid,$token)
    {
        $data_string = 
        '{
                "state": "disabled",
                "products": [
                    "'.$buyrproductid.'"
                ]
        }';

        $ApiUrls = $this->getApiUrls();


        $ch = curl_init($ApiUrls['disablePricingModelProduct']);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string),
            'authorization:'.$token)                                                                       
        );                                                                                                                   
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
        curl_setopt($ch, CURLOPT_HEADER, true);
        $result = curl_exec($ch);
        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

    }
    public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }
}