<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Productmanagement;
use Magento\Framework\Controller\ResultFactory;

class Editmyview extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
         $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
         $connection = $resource->getConnection();
         $tableName = $resource->getTableName('buyr_seller_view');

         $insertview = "update buyr_seller_view set view_name='".$_REQUEST['view_name']."',  idstarting='".$_REQUEST['idstarting']."',  idending='".$_REQUEST['idending']."', pricestarting='".$_REQUEST['pricestarting']."', priceending='".$_REQUEST['priceending']."',  qtystarting='".$_REQUEST['qtystarting']."', qtyending='".$_REQUEST['qtyending']."', visibility='".$_REQUEST['visibility']."', attributeset='".$_REQUEST['attributeset']."' where id='".$_REQUEST['view_id']."'";
         $connection->query($insertview);

         //$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
         //$path = 'buyr/productmanagement/index/idstarting/'.$_REQUEST['idstarting'].'/idending/'.$_REQUEST['idending'].'/pricestarting/'.$_REQUEST['pricestarting'].'/priceending/'.$_REQUEST['priceending'].'/qtystarting/'.$_REQUEST['qtystarting'].'/qtyending/'.$_REQUEST['qtyending'].'/visibility/'.$_REQUEST['visibility'].'/attributeset/'.$_REQUEST['attributeset'];
         //$resultRedirect->setPath($path);
         //return $resultRedirect;
	}
}