<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Productmanagement;
use Magento\Framework\Controller\ResultFactory;

class Doneproduct extends \Magento\Backend\App\Action
{
  protected $resultPageFactory = false;
  public function __construct(
    \Magento\Backend\App\Action\Context $context,
    \Magento\Framework\View\Result\PageFactory $resultPageFactory
  ) {
    parent::__construct($context);
    $this->resultPageFactory = $resultPageFactory;
  }

  public function execute()
  {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $object->get('Magento\Customer\Model\Session');
        if($session->getAuthenticationKey() != "")
        {
          $login = $session->getAuthenticationKey();
        }
        else
        {
          $login = "false";
        }

        if($login != "false")
        {

            $result = $this->publish($login,$_REQUEST);

            if($result == 1)
            {
               $msg = "published";
            }
            else if($result == 2)
            {
               $msg = "updated";
            }
            else
            {
              $msg = "error";
            }

            
        }  
        else
        {
            $msg = "Session expire";
        }

        echo $msg;
  }
  public function getRetailarId()
  {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('buyr_seller_info');
        $select_retailar_info = "select * from buyr_seller_info";
        $result_retailar_info = $connection->fetchAll($select_retailar_info);
        foreach($result_retailar_info as $result_retailar_info_new)
        { 
          $retailar_id = $result_retailar_info_new["buyr_retailer_id"];
        }

        return $retailar_id;
  }

  public function getAllProductAttributes($productid)
  {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $ptoductAttributes = $_helper->getAllAttributesInJson($productid);

        return $ptoductAttributes;
  }

  public function publish($token,$productinfo)
  {
        $_product = $this->getProductInfo($productinfo['product_id']);
        $productdescription  = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_product->getDescription()))))));
        $manufacture = $_product->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($_product);
        $productcategories = $this->getProductCategories($productinfo['product_id']);

        $_product = $this->getProductInfo($productinfo['product_id']);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseurl = $storeManager->getStore()->getBaseUrl();       


        $allimages = $_product->getMediaGallery('images');
        $allimagesnew = array();
        foreach ($allimages as $allimages_new) {
            $allimagesnew[] = $baseurl.'pub/media/catalog/product'.$allimages_new['file'];
        }

        $allimagesfinal = implode('","',$allimagesnew);
        $allimagesfinal = '"'.$allimagesfinal.'"';

        $retailarid = $this->getRetailarId();

        
        $productAttributes = $this->getAllProductAttributes($productinfo['product_id']);

        $data_string = '{
                    "originId": "'.$_product->getSku().'",
                    "originBreadCrumb": "'.$_product->getSku().'",
                    "name": "'.$_product->getName().'",
                    "catalogId": "0",
                    '.$productAttributes.'
                    "retailerId": "'.$retailarid.'",
                    "url": "'.$_product->getProductUrl().'",
                    "catalogName": "'.$productcategories.'",
                    "description": "'.$productdescription.'",
                    "images": [
                    '.$allimagesfinal.'
                    ],

                    "sku": "'.$_product->getSku().'",
                    "price": '.$_product->getPrice().',
                    "brand": "'.$manufacture.'"
                }';                                                                                               

        $ApiUrls = $this->getApiUrls();

        $ch = curl_init($ApiUrls['publishProduct']);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string),
            'authorization:'.$token)                                                                       
        );                                                                                                                   
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
        curl_setopt($ch, CURLOPT_HEADER, false);
        $result = curl_exec($ch);
        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($httpcode_patch == 200)
        {
                    $jsonResult = json_decode($result, true);
                    $buyr_product_id = $jsonResult['@id'];

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                    $connection = $resource->getConnection();
                    $tableName = $resource->getTableName('buyr_published_products');

                    $check_product_exist_query = "select * from buyr_published_products where product_id='".$_REQUEST['product_id']."'";
                    $get_exist_product = $connection->fetchAll($check_product_exist_query);
                    $rowcount = count($get_exist_product);
                    if($rowcount == 0)
                    {
                        $this->disablePricingModel($buyr_product_id,$token);
                    }

                    $sql = "insert into buyr_published_products set product_id='".$_REQUEST['product_id']."', buyr_product_id='".$buyr_product_id."', published_date=NOW(), status=1";
                    $connection->query($sql);

                    
                    $this->postPricingModel($token,$buyr_product_id,$_REQUEST);

                    $responseresult = 1;
        }
        else if($httpcode_patch == 500)
        {
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                    $connection = $resource->getConnection();
                    $tableName = $resource->getTableName('buyr_published_products');

                    $sql = "select * from buyr_published_products where product_id='".$_REQUEST['product_id']."'";

                    $rows = $connection->fetchAll($sql); 
                    foreach ($rows as $rows_new) {
                    $buyr_product_id = $rows_new['buyr_product_id'];
                    }
                    
                    $this->postPricingModel($token,$buyr_product_id,$_REQUEST);
                    $enablestatus = $this->enableproduct($token,$buyr_product_id,$_REQUEST);
                    if($enablestatus == "success")
                    {
                       $responseresult = 2;   
                    }   
                    else
                    {
                       $responseresult = 3;
                    }         
        }
        else
        {
             $responseresult = 4;
        }

        return $responseresult;

  }
    public function getProductCategories($productid)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
        $categories = $_product->getCategoryIds();
        $categorylist = "";
        foreach($categories as $category){
            $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
            $categorylist = $categorylist.",".$cat->getName();
        }
        $categorylist = ltrim($categorylist,',');
        return $categorylist;
    }

    public function enableproduct($token,$buyrproductid,$productinfo)
    {
        $_product = $this->getProductInfo($productinfo['product_id']);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseurl = $storeManager->getStore()->getBaseUrl();
           


        $allimages = $_product->getMediaGallery('images');
        $allimagesnew = array();
        foreach ($allimages as $allimages_new) {
            $allimagesnew[] = $baseurl.'pub/media/catalog/product'.$allimages_new['file'];
        }

        $allimagesfinal = implode('","',$allimagesnew);
        $allimagesfinal = '"'.$allimagesfinal.'"';

        if($productinfo['product_quantity'] != "") { $product_quantity = $productinfo['product_quantity']; } else { $product_quantity = 0; }
        if($productinfo['product_cost'] != "") { $product_cost = $productinfo['product_cost']; } else { $product_cost = 0; }
        if($productinfo['product_msrp'] != "") { $product_msrp = $productinfo['product_msrp']; } else { $product_msrp = 0; }
        if($productinfo['starting_pricing'] != "") { $starting_pricing = $productinfo['starting_pricing']; } else { $starting_pricing = 0; }
        if($productinfo['map_price'] != "") { $map_price = $productinfo['map_price']; } else { $map_price = 0; }
        if($productinfo['price_change'] != "") { $price_change = $productinfo['price_change']; } else { $price_change = 0; }

        

        $productAttributes = $this->getAllProductAttributes($productinfo['product_id']);

        $data_string = '{
            "name": "'.$_product->getName().'",
            "catalogId": "0",
            '.$productAttributes.'
            "retailerId": "'.$retailarid.'",
            "url": "'.$_product->getProductUrl().'",
            "catalogName": "'.$productcategories.'",
            "description": "'.$productdescription.'",
            "images": [
            '.$allimagesfinal.'
            ],

            "sku": "'.$_product->getSku().'",
            "price": '.$_product->getPrice().',
            "brand": "'.$manufacture.'"
        }';

        $ApiUrls = $this->getApiUrls();                                                                                                
                                                                                                                                                     
        $ch = curl_init($ApiUrls['publishProduct'].'/'.$buyrproductid);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string),
            'authorization:'.$token)                                                                       
        );                                                                                                                   
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
        curl_setopt($ch, CURLOPT_HEADER, true);
        $result = curl_exec($ch);
        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($httpcode_patch == 200)
        {

             $result = "success";
        }
        else
        {
            $result = "false";
        }

        return $result;

    }

    public function getProductInfo($productid)
    {
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
          
          return $_product;
    }
    public function postPricingModel($token,$buyrproductid,$productinfo)
    {
        $_product = $this->getProductInfo($productinfo['product_id']);

        if($productinfo['product_quantity'] != "") { $product_quantity = $productinfo['product_quantity']; } else { $product_quantity = 0; }
        if($productinfo['product_cost'] != "") { $product_cost = $productinfo['product_cost']; } else { $product_cost = 0; }
        if($productinfo['product_msrp'] != "") { $product_msrp = $productinfo['product_msrp']; } else { $product_msrp = 0; }
        if($productinfo['starting_pricing'] != "") { $starting_pricing = $productinfo['starting_pricing']; } else { $starting_pricing = 0; }
        if($productinfo['map_price'] != "") { $map_price = $productinfo['map_price']; } else { $map_price = 0; }
        if($productinfo['price_change'] != "") { $price_change = $productinfo['price_change']; } else { $price_change = 0; }

       $data_string = 
        '{
          "units": '.$product_quantity.',
          "cost": '.$product_cost.',
          "msrp": '.$product_msrp.',
          "startingPrice": '.$starting_pricing.',
          "mapPrice": '.$map_price.',
          "shippingCost": 0,
          "priceChange": '.$price_change.',
          "priceModel": "'.$productinfo['select_pricing'].'",
          "timeframeType": "'.$productinfo['timeframe'].'",
          "timeframePeriod": "'.$productinfo['timeday'].'",
          "timeframeValue": '.$productinfo['time_frame_value'].'
        }';

        if($productinfo['select_pricing'] == "Tiered")
        {
          $prices = "";
          for($i=0;$i < count($productinfo['tire_price_details']);$i++)
          {
            $qwe = '{"min":'.$productinfo['tire_price_details'][$i]['min'].',"max":'.$productinfo['tire_price_details'][$i]['max'].',"price":'.$productinfo['tire_price_details'][$i]['price'].'}';
            if($i==0)
            {
            $prices = $prices.$qwe;
            }
            else
            {
            $prices = $prices.",".$qwe;
            }
          }

          //echo $prices;
          //exit();
          
          $data_string = 
          '{
            "units": '.$product_quantity.',
            "cost": '.$product_cost.',
            "msrp": '.$product_msrp.',
            "startingPrice": '.$starting_pricing.',
            "mapPrice": '.$map_price.',
            "shippingCost": 0,
            "priceChange": '.$price_change.',
            "priceModel": "'.$productinfo['select_pricing'].'",
            "tiers":['.$prices.'],
            "timeframeType": "'.$productinfo['timeframe'].'",
            "timeframePeriod": "'.$productinfo['timeday'].'",
            "timeframeValue": '.$productinfo['time_frame_value'].'
          }';
        }

        $ApiUrls = $this->getApiUrls();

        $ch = curl_init($ApiUrls['pricingModelConfig'].'/'.$buyrproductid);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string),
            'authorization:'.$token)                                                                       
        );                                                                                                                   
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
        curl_setopt($ch, CURLOPT_HEADER, true);
        $result = curl_exec($ch);
        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

    }

    public function disablePricingModel($buyrproductid,$token)
    {
        $data_string = 
        '{
                "state": "disabled",
                "products": [
                    "'.$buyrproductid.'"
                ]
        }';

        $ApiUrls = $this->getApiUrls();


        $ch = curl_init($ApiUrls['disablePricingModelProduct']);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string),
            'authorization:'.$token)                                                                       
        );                                                                                                                   
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
        curl_setopt($ch, CURLOPT_HEADER, true);
        $result = curl_exec($ch);
        $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

    }

    public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }
}