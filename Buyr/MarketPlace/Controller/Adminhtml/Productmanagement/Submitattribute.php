<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Productmanagement;
use Magento\Framework\Controller\ResultFactory;

class Submitattribute extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        if(count($_REQUEST) == 2)
        {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('buyr_attribute_column');

            //$truncatesql = "truncate table buyr_attribute_column";
            //$connection->query($truncatesql);
            $updatestatus = "update buyr_attribute_column set status=0";
            $connection->query($updatestatus);

            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('buyr/productmanagement/index');
            return $resultRedirect;
        }
        else
        {
            if(isset($_REQUEST['columnattribute']))
            {
                $selected_attributes = $_REQUEST['columnattribute'];
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();
                $tableName = $resource->getTableName('buyr_attribute_column');

                //$truncatesql = "truncate table buyr_attribute_column";
                //$connection->query($truncatesql);

                for($i=0;$i < count($selected_attributes);$i++)
                {
                        $deleteattribute = "delete from buyr_attribute_column where column_code='".$selected_attributes[$i]."'";
                        $connection->query($deleteattribute);

                        $insertattribute = "insert into buyr_attribute_column set column_code='".$selected_attributes[$i]."', status=1";
                        $connection->query($insertattribute);
                }

                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('buyr/productmanagement/index');
                return $resultRedirect;
            
            }
        }
    }
}