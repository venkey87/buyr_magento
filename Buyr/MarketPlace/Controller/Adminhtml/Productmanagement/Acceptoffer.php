<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Productmanagement;
use Magento\Framework\Controller\ResultFactory;

class Acceptoffer extends \Magento\Backend\App\Action
{
  protected $resultPageFactory = false;
  public function __construct(
    \Magento\Backend\App\Action\Context $context,
    \Magento\Framework\View\Result\PageFactory $resultPageFactory
  ) {
    parent::__construct($context);
    $this->resultPageFactory = $resultPageFactory;
  }

  public function execute()
  {
        //echo $_REQUEST['buyrpid'];
        
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $object->get('Magento\Customer\Model\Session');
        if($session->getAuthenticationKey() != "")
        {
          $login = $session->getAuthenticationKey();
        }
        else
        {
          $login = "false";
        }

        if($login != "false")
        {
            //$msg = $_REQUEST['offerid'];

            $result = $this->acceptOffer($login,$_REQUEST['offerid']);

            if($result != "false")
            {
              $msg = "success";
            }

            
        }  
        else
        {
            $msg = "Session expire";
        }
    
     
        print_r($msg);
  }

  public function acceptOffer($authentication_key,$offerid)
  {
            $ApiUrls = $this->getApiUrls();

            $ch = curl_init($ApiUrls['acceptOffer'].'/'.$offerid.'/accept');                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");                                                                                                                                
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',
                'authorization:'.$authentication_key)                                                                       
            );                                                                                                                   
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
            $result = curl_exec($ch);
            $httpcode_patch = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $httpcode_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $result = json_decode($result,true);

            if($httpcode_status == 200)
            {
              $response = $result;         
            }
            else
            {
              $response = "false";
            }


            return $response;

            //print_r($response);
            //exit();
            //echo json_encode($response);
  }

  public function getApiUrls()
  {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
  }
  
}