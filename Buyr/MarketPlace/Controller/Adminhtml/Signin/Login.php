<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Signin;

class Login extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
        if(isset($_REQUEST['password']) && isset($_REQUEST['email']))
        {
        	$login = $this->login($_REQUEST['email'],$_REQUEST['password']);
	        if($login != "false")
	        {
                $object = \Magento\Framework\App\ObjectManager::getInstance();
				$session = $object->get('Magento\Customer\Model\Session');
				$session->unsAuthenticationKey();
				$session->setAuthenticationKey($login);

				$response = "success";
	        }
	        else
	        {
	        	$response = "unsuccess";
	        }
	    }

	    echo $response;
	}
	public function login($username,$password)
	{
	        $datasignin = array("email" => $username, "password" => $password);                                                                    
	        $data_string_signin = json_encode($datasignin);    

			$ApiUrls = $this->getApiUrls();                                                                               

	        $ch_signin = curl_init($ApiUrls['signIn']);                                                                      
	        curl_setopt($ch_signin, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	        curl_setopt($ch_signin, CURLOPT_POSTFIELDS, $data_string_signin);                                                                  
	        curl_setopt($ch_signin, CURLOPT_RETURNTRANSFER, true);                                                                      
	        curl_setopt($ch_signin, CURLOPT_HTTPHEADER, array(                                                                          
	        'Content-Type: application/json',                                                                                
	        'Content-Length: ' . strlen($data_string_signin))                                                                       
	        );                                                                                                                   
	        curl_setopt($ch_signin,CURLOPT_RETURNTRANSFER,TRUE);                                                                                                                    
	        curl_setopt($ch_signin, CURLOPT_HEADER, true);

	        $result_signin = curl_exec($ch_signin);

	        //exit();

	        $httpcode_signin = curl_getinfo($ch_signin, CURLINFO_HTTP_CODE);
	        $headerLen = curl_getinfo($ch_signin, CURLINFO_HEADER_SIZE); 
	        $curlBody = substr($result_signin, $headerLen);
	        
	        if($httpcode_signin == 200)
	        {
	            if (!curl_errno($ch_signin)) {
	              $headers=array();
	              $data_header=explode("\n",$result_signin);

	              
                 
	              $authorization = $data_header[3];
	              $authentication_token = trim(substr($authorization,14));

				  $this->updateDitails($curlBody,$authentication_token);
		        }
		        else
		        {
		        $authentication_token = "false";
		        }
		        curl_close($ch_signin);

		        
	        }
	        else
	        {
	            $authentication_token = "false";
	        }
	        
	        return $authentication_token;
	}
	public function updateDitails($responce_data,$authentication_token)
	{
		$obj = json_decode($responce_data,true);

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $target_dir  =  $directory->getRoot().'/image/';
		if(!is_dir($target_dir))
		{
            mkdir($target_dir);
		}
		chmod($target_dir, 0777);

        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('buyr_seller_info');


        if(isset($obj['businessInfo']['officialAddress']['addressCity'])) { $city = $obj['businessInfo']['officialAddress']['addressCity']; } else { $city=""; }
        if(isset($obj['businessInfo']['tradeAddress']['addressCity'])) { $product_origin_city = $obj['businessInfo']['tradeAddress']['addressCity']; } else { $product_origin_city=""; }
        if(isset($obj['creditInformation']['nameOnCard'])) { $nameOnCard = $obj['creditInformation']['nameOnCard']; } else { $nameOnCard=""; }
        if(isset($obj['creditInformation']['cardNumber'])) { $cardNumber = $obj['creditInformation']['cardNumber']; } else { $cardNumber=""; }

        $truncatesql = "truncate table buyr_seller_info";
        $connection->query($truncatesql);
        $sql = "insert into buyr_seller_info set authentication_key='".$authentication_token."', email='".$obj['email']."', firstname='".$obj['first']."', lastname='".$obj['last']."', buyr_retailer_id='".$obj['@id']."', entity_name='".$obj['businessInfo']['legalName']."', trade_name='".$obj['businessInfo']['tradeName']."', ein_number='".$obj['businessInfo']['EIN_Number']."', business_phone='".$obj['businessInfo']['businessPhone']."', adminPhone='".$obj['businessInfo']['adminPhone']."', address_line_1='".$obj['businessInfo']['officialAddress']['streetAddress']."', address_line_2='".$obj['businessInfo']['officialAddress']['addressLocality']."', city='".$city."', state='".$obj['businessInfo']['officialAddress']['addressRegion']."', zip='".$obj['businessInfo']['officialAddress']['postalCode']."', product_origin_address_line_1='".$obj['businessInfo']['tradeAddress']['streetAddress']."', product_origin_address_line_2='".$obj['businessInfo']['tradeAddress']['addressLocality']."', product_origin_city='".$product_origin_city."', product_origin_state='".$obj['businessInfo']['tradeAddress']['addressRegion']."', product_origin_zip='".$obj['businessInfo']['tradeAddress']['postalCode']."', routing_number='".$obj['bankingInformation']['routingNumber']."', account_number='".$obj['bankingInformation']['accountNumber']."', name_on_card='".$nameOnCard."', card_number='".$cardNumber."', subcription_model='', retailer_image='".$obj['image']."', created_at=NOW()";

        $connection->query($sql);
	}

	public function getApiUrls()
    {
        $object = \Magento\Framework\App\ObjectManager::getInstance();
        $_helper = $object->get('Buyr\MarketPlace\Helper\Data');
        $apiUrls = $_helper->BuyrApiUrls();

        return $apiUrls;
    }

}