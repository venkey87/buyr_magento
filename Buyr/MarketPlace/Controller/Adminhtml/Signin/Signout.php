<?php
namespace Buyr\MarketPlace\Controller\Adminhtml\Signin;
use Magento\Framework\Controller\ResultFactory;

class Signout extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
                $object = \Magento\Framework\App\ObjectManager::getInstance();
				$session = $object->get('Magento\Customer\Model\Session');
				$resource = $object->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();
				$delete_authentication_key = "update buyr_seller_info set authentication_key=''";
				$connection->query($delete_authentication_key);
				//if($session->getAuthenticationKey() == "")
				//{
				   $session->unsAuthenticationKey();
			    //}
			    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('buyr/signin/index');
                return $resultRedirect;
	}
}