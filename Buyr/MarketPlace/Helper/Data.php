<?php
namespace Buyr\MarketPlace\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
//use Buyr\MarketPlace\Helper\ConfigHelper;

class Data extends AbstractHelper
{
       public function BuyrApiUrls()
       {
               $urls = array(
                   "publishProduct" => "http://dev-sync.buyr.com:8080/api/v1/product",
                   "pricingModelConfig" => "http://dev-sync.buyr.com:8080/api/v1/prices/model/product",
                   "acceptOffer" => "http://dev-sync.buyr.com/api/v1/profile/offer",
                   "getOffer" => "http://dev-sync.buyr.com:8080/api/v1/profile/offers/product",
                   "pricingModelCompute" => "http://dev-sync.buyr.com:8080/api/v1/prices/model/compute",
                   "disablePricingModelProduct" => "http://dev-sync.buyr.com:8080/api/v1/prices/model/product/state",
                   "profile" => "http://dev-sync.buyr.com:8080/api/v1/profile",
                   "signUp" => "http://dev-sync.buyr.com:8080/api/v1/signUp",
                   "signIn" => "http://dev-sync.buyr.com:8080/api/v1/signIn",
                   "offerCount" => "http://dev-sync.buyr.com:8080/api/v1/profile/offers/counts",
                   "sendApiUser" => "http://dev-sync.buyr.com:8080/api/v1/sync/connection"
                   );
               return $urls;
       }
       public function getAllAttributesInJson($productid)
       {
            $product_attributes = $this->getAllAttributes($productid);

            $attrString = "";
            
            $attributesorg = array('activity','features_bags','material','strap_bags','style_bags','upc','color','cost','country_of_manufacture','custom_design_from','custom_design_to','manufacturer','meta_description','meta_keyword','meta_title','minimal_price','msrp','short_description','size','special_price','tax_class_id','url_key','visibility','weight');
            for($i=0;$i < count($product_attributes);$i++)
            {
                if(in_array($product_attributes[$i]['code'], $attributesorg))
                {
                    $values = implode(",",(array)$product_attributes[$i]['value']);

                    $attrString = $attrString.'"'.$product_attributes[$i]['code'].'":"'.$values.'",';

                }
            }

            return $attrString;
       }
       public function getAllAttributes($productid)
       {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $productattributes = "select attribute_code as code from eav_attribute where entity_type_id=4 and backend_type in ('text','varchar','decimal','datetime','datetime')";
            $rows = $connection->fetchAll($productattributes);
            $attributes = array();
            $i=0;
            foreach ($rows as $rows_new) {
                  $attr_value = $this->getAttributeValue($productid,$rows_new['code']);
                  if(!empty($attr_value))
                  {
                    $attributes[$i]['code'] = $rows_new['code'];
                    $attributes[$i]['value'] = $attr_value;
                    $i++;
                  }
            }
            return $attributes;
       }
       public function getAttributeValue($productid,$attricutecode)
       {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
            $value = $product->getData($attricutecode);
            $attr = $product->getResource()->getAttribute($attricutecode);
            if ($attr->usesSource()) {
                $value = $attr->getSource()->getOptionText($value);
            }

            return $value;

       }
}